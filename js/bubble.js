/**
 * Author: Juan Antonio Aldea Armenteros
 * Date: 5/10/13
 * Time: 1:07 PM
 */

Bubble = PhysicsObject.extend({
    isSplit: false,

    init: function (bubble) {
        var fixDef = new b2FixtureDef();
        fixDef.density = 1.0;
        fixDef.friction = 0;
        fixDef.restitution = 1.0;

        var bodyDef = new b2BodyDef();
        bodyDef.type = b2Body.b2_dynamicBody;
        fixDef.shape = new b2CircleShape(bubble.radius * GameWorld.scaled_height);

        bodyDef.position.x = bubble.position.x;
        bodyDef.position.y = bubble.position.y;


        bubble.object = this;
        bubble.name = bubble.name ? bubble.name : "bubble";

        bodyDef.userData = copy(bubble);

        this.parent(bodyDef, fixDef);

        this.body.ApplyImpulse(new b2Vec2(bubble.impulse.x, bubble.impulse.y), this.body.GetWorldCenter());
    },

    update: function () {

    },


    split: function () {
        this.isSplit = true;
        var bubble = this.bodyDef.userData;
        bubble.radius /= 2.0;
        GameWorld.spawnBubble(bubble);
        //GameWorld.spawnBubble(bubble);
    },

    onCollision: function (other) {
        if (other == "rope" && !this.isSplit) {
            if (this.bodyDef.userData.lives > 0) {
                this.bodyDef.userData.lives--;
                this.split();
            }
            GameWorld.DestroyBody(this.body);
            RM.playsound("explosion");
        }
    }
});
