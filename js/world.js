World = Class.extend({
    canvas: null,

    width: window.innerWidth,

    height: window.innerHeight,

    scale: 30.0,

    scaled_width: 0,

    scaled_height: 0,

    world: null,

    gravity: new b2Vec2(0, 20),

    bodiesToDestroy: [],
    lastStep: Date.now(),

    newBubbles: [],

    init: function () {
        this.canvas = document.getElementById("canvas");
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.scaled_width = this.width / this.scale;
        this.scaled_height = this.height / this.scale;
        this.world = new b2World(this.gravity, false);

        var listener = new Box2D.Dynamics.b2ContactListener;

        listener.BeginContact = function (contact) {
            function compare(type1, type2, obj1, obj2) {
                return type1 == obj1 && type2 == obj2 || type1 == obj2 && type2 == obj1;
            }

            var uDA = contact.m_fixtureA.GetBody().GetUserData();
            var uDB = contact.m_fixtureB.GetBody().GetUserData();
            uDA = uDA['name'];
            uDB = uDB['name'];
            if (uDA != null && uDB != null) {
                if (compare("rope", "block", uDA, uDB)) {
                    //console.log("pollaca");
                }

            }
        };

        listener.EndContact = function (contact) {
            function compare(type1, type2, obj1, obj2) {
                return type1 == obj1 && type2 == obj2 || type1 == obj2 && type2 == obj1;
            }

            var uDA = contact.m_fixtureA.GetBody().GetUserData();
            var uDB = contact.m_fixtureB.GetBody().GetUserData();
            uDA = uDA['name'];
            uDB = uDB['name'];

            if (uDA != null && uDB != null) {
                if (compare("block", "rope", uDA, uDB)) {
                    //console.log("pollaca");
                }
            }
        };

        listener.PostSolve = function (contact, impulse) {
            function compare(type1, type2, obj1, obj2) {
                return type1 == obj1 && type2 == obj2 || type1 == obj2 && type2 == obj1;
            }

            var uDA = contact.m_fixtureA.GetBody().GetUserData();
            var uDB = contact.m_fixtureB.GetBody().GetUserData();
            if (uDA != null && uDB != null) {
                uDA = uDA['name'];
                uDB = uDB['name'];
                if (compare("rope", "bubble", uDA, uDB)) {
                    console.log("pollaca");
                }
            }
        };

        listener.PreSolve = function (contact, oldManifold) {
            function compare(type1, type2, obj1, obj2) {
                return type1 == obj1 && type2 == obj2 || type1 == obj2 && type2 == obj1;
            }

            var uDA = contact.m_fixtureA.GetBody().GetUserData();
            var uDB = contact.m_fixtureB.GetBody().GetUserData();
            if (uDA != null && uDB != null) {
                uDA = uDA['name'];
                uDB = uDB['name'];
                if (compare("rope", "rope", uDA, uDB)) {
                    contact.SetEnabled(false);
                } else if (compare("rope", "player", uDA, uDB)) {
                    contact.SetEnabled(false);
                } else if (compare("player", "bubble", uDA, uDB)) {
                    player.onCollision("bubble");
                    contact.SetEnabled(false);
                    //} else if (compare("player", "block-floor", uDA, uDB)) {
                    //} else if (compare("rope", "block", uDA, uDB)) {
                    //} else if (compare("rope", "bubble", uDA, uDB)) {
                } else {
                    contact.m_fixtureA.GetBody().GetUserData().object.onCollision(uDB);
                    contact.m_fixtureB.GetBody().GetUserData().object.onCollision(uDA);
                    //contact.SetEnabled(false);
                    //console.log(uDA, uDB);
                }
            }

        };
        this.world.SetContactListener(listener);
    },

    CreateBody: function (bodyDef) {
        return this.world.CreateBody(bodyDef);
    },

    DestroyBody: function (body) {
        if (this.bodiesToDestroy.indexOf(body) == -1) {
            this.bodiesToDestroy.push(body);
        }
    },

    spawnBubble: function (object) {
        this.newBubbles.push(object);
    },

    update: function () {
        var currentTime = Date.now();
        var dt = currentTime - this.lastStep;
        var steps = Math.floor(dt / 16.667);
        this.lastStep += steps * 16.667;
        for (var i = 0; i < steps; i++) {
            this.world.Step(1.0 / 60.0, 10, 10);
            //console.log("steps", i);
        }

        this.world.DrawDebugData();
        this.world.ClearForces();

        for (var i = 0; i < this.newBubbles.length; i++) {
            new Bubble(this.newBubbles[i]);
            this.newBubbles[i].position.x = 0.5;
            this.newBubbles[i].position.y = 0.5;
            new Bubble(this.newBubbles[i]);
        }

        this.newBubbles = [];

        for (var i = 0; i < this.bodiesToDestroy.length; i++) {
            this.world.DestroyBody(this.bodiesToDestroy[i]);
            delete this.bodiesToDestroy[i];
        }

        this.bodiesToDestroy = [];
    }
});
